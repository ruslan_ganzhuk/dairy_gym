CREATE TABLE user(
  id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  login VARCHAR(50) NOT NULL ,
  password VARCHAR(50) NOT NULL,
  name VARCHAR(50) NOT NULL ,
  surname VARCHAR(60),
  famale ENUM ('MEN','WOMEN','KIND'),
  email VARCHAR(80) NOT NULL,
  number VARCHAR(20),
  level ENUM ('STARTER','JUNIOR','MIDDLE','PROFI'),
  data_registration DATE,
  weight INT,
  growth INT
) CHARACTER SET utf8;

CREATE TABLE exersice(
 id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
 name VARCHAR(40) NOT NULL ,
 description VARCHAR(1000) NOT NULL,
 icon VARCHAR(60),
 type ENUM('CHEST','LEGS','BACK','TRICEPS','BICEPS','SHOULDERS','CARDIO') NOT NULL ,
 date_of_comlit DATE NOT NULL,
 note VARCHAR(200),
 weight INT,
 num_of_approaches INT,
 result_id BIGINT,
 FOREIGN KEY (result_id) REFERENCES result(id)
)CHARACTER SET utf8;

CREATE TABLE result(
 id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 all_weight BIGINT,
 all_distance BIGINT,
 progress INT,
 ex_id BIGINT,
 FOREIGN KEY (ex_id) REFERENCES exersice (id)
)CHARACTER SET utf8;

CREATE TABLE programm(
 id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY ,
 name VARCHAR(50) NOT NULL

)CHARACTER SET utf8;

CREATE TABLE program_exersice(
 program_id BIGINT NOT NULL ,
 ex_id BIGINT NOT NULL ,
 FOREIGN KEY (program_id) REFERENCES programm(id),
 FOREIGN KEY (ex_id) REFERENCES exersice(id)
)CHARACTER SET utf8;
